#ifndef INDENT_WILDEXP_H
#define INDENT_WILDEXP_H

#if defined (_WIN32) && !defined (__CYGWIN__)
void wildexp(int *argc, char ***argv);
#endif

#endif /* INDENT_WILDEXP_H */
